use std::path::Path;
use rusqlite::Connection;
use rusqlite::params;


pub fn connect_to_database<T: AsRef<Path>>(path: T) -> Connection {
    let connection = Connection::open(path).unwrap();
    connection
}

pub fn init_database() -> Connection {
    let database_path = Path::new("./database.db");
    let conn = connect_to_database(&database_path);
    let result1 = conn.execute(
    	"CREATE TABLE authors (
                      id    INTEGER PRIMARY KEY,
                      first_name  TEXT NOT NULL,
                      last_name  TEXT NOT NULL,

        )",
    	params![],
    );
    let result2 = conn.execute(
	"CREATE TABLE books (
                      id        INTEGER PRIMARY KEY,

        )",
        params![],
    );
    
    conn
}

pub fn test() -> i8 {
    5
}
